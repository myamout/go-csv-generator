package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// ConfigList ...
// Parent JSON structure for the generator
type ConfigList struct {
	RowCount    int      `json:"rowCount"`
	OutfileName string   `json:"outfileName"`
	DataSet     []Config `json:"dataSet"`
}

// Config ...
type Config struct {
	ColumnName     string   `json:"columnName"`
	ValueType      string   `json:"valueType"`
	AllowedValues  []string `json:"allowedValues"`
	MinRange       float64  `json:"minRange"`
	MaxRange       float64  `json:"maxRange"`
	InsertEmptyRow bool     `json:"insertEmptyRow"`
}

// JSONResponse ...
type JSONResponse struct {
	ColumnNames []string   `json:"columnNames"`
	Data        [][]string `json:"data"`
}

// ExtractColumnNamesAndConfig ...
func ExtractColumnNamesAndConfig(configList ConfigList) ([]string, []Config) {
	columnNames := make([]string, len(configList.DataSet))

	for index := 0; index < len(configList.DataSet); index++ {
		columnNames[index] = configList.DataSet[index].ColumnName
	}

	return columnNames, configList.DataSet
}

// ValidateAndMapConfig ...
func ValidateAndMapConfig(configPath string) {
	var configList ConfigList
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(data, &configList)
	if err != nil {
		fmt.Println(err)
	}

}

func validate(configList ConfigList) bool {
	valid := true
	if len(configList.DataSet) == 0 {
		fmt.Println("No cell configs provided")
		valid = false
	}
	if configList.OutfileName == "" {
		fmt.Println("No outfile name provided")
		valid = false
	}
	if configList.RowCount == 0 {
		fmt.Println("Warning: Row count is zero, nothing will be written to CSV")
	}
	return valid
}
