module gitlab.com/go/csv/generator

go 1.13

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
)
