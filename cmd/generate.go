package cmd

/*
Copyright © 2020 Matthew Yamout gitlab.com/myamout

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import (
	"github.com/spf13/cobra"
	"gitlab.com/go/csv/generator/cmdfunctions"
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:     "generate",
	Aliases: []string{"gen"},
	Short:   "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Validate CSV Config
		cmdfunctions.GenerateMain(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)
	generateCmd.Flags().String("configPath", "./config.json", "Path to config JSON file for CSV generation")
	generateCmd.Flags().Bool("logRowGeneration", false, "Log each generate row to the console for inspection during generation")
	generateCmd.Flags().Bool("serveData", false, "Serve generated CSV data over rest")
	generateCmd.Flags().String("dataEndpoint", "data", "Endpoint to serve CSV data at")
}
