package cmdfunctions

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/go/csv/generator/models"
)

// GenerateMain ...
// Generates the CSV based on the user defined config file.
// After CSV generation an http server is started based on
// if --serveData=true is supplied at runtime.
func GenerateMain(cmd *cobra.Command, args []string) {
	configPath, _ := cmd.Flags().GetString("configPath")
	rand.Seed(time.Now().UnixNano())
	var configList models.ConfigList
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(data, &configList)
	if err != nil {
		fmt.Println(err)
	}
	models.ValidateAndMapConfig(configPath)
	columnNames, configs := models.ExtractColumnNamesAndConfig(configList)
	csvContents := generateCsv(columnNames, configs, configList.RowCount, configList.OutfileName)
	fmt.Println("CSV File generated")

	shouldServe, _ := cmd.Flags().GetBool("serveData")
	endpointName, _ := cmd.Flags().GetString("dataEndpoint")

	if shouldServe {
		http.HandleFunc(fmt.Sprintf("/%s", endpointName), func(w http.ResponseWriter, r *http.Request) {
			response := models.JSONResponse{
				ColumnNames: columnNames,
				Data:        csvContents,
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(response)
		})
		fmt.Printf("HTTP Server started on port 8080: http://localhost:8080/%s\n", endpointName)
		http.ListenAndServe(":8080", nil)
	}
}

// Generates all of the CSV rows and writes out to a CSV
// file standard format. Column names in the first row and
// each following row containing the data.
func generateCsv(columnNames []string, configs []models.Config, rowCount int, outfileName string) [][]string {
	file, err := os.Create(outfileName)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	// Generate values
	csvContents := make([][]string, rowCount)
	for index := 0; index < rowCount; index++ {
		csvContents[index] = generateCsvRow(configs)
	}
	err = writer.Write(columnNames)
	if err != nil {
		fmt.Println(err)
	}
	err = writer.WriteAll(csvContents)
	if err != nil {
		fmt.Println(err)
	}

	return csvContents
}

// Generates a CSV row based on the given config
// defined by the user.
func generateCsvRow(configs []models.Config) []string {
	csvRow := make([]string, len(configs))
	index := 0
	for _, v := range configs {
		shouldCreateCell := true
		if v.InsertEmptyRow == true {
			shouldCreateCell = rand.Float32() < 0.5
		}

		if shouldCreateCell {
			if v.ValueType == "String" {
				csvRow[index] = generateRandomStringCell(v.AllowedValues)
			} else if v.ValueType == "Number" {
				csvRow[index] = generateRandomNumberCell(v.MinRange, v.MaxRange)
			}
		} else {
			csvRow[index] = ""
		}
		index++
	}
	return csvRow
}

func generateRandomNumberCell(minRange float64, maxRange float64) string {
	return fmt.Sprintf("%.2f", minRange+rand.Float64()*(maxRange-minRange))
}

func generateRandomStringCell(allowedValues []string) string {
	return allowedValues[rand.Intn(len(allowedValues))]
}
